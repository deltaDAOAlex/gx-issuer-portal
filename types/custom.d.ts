// file-loader
declare module '*.svg' {
  const content: any
  export default content
}

// url-loader
declare module '*.svg?data' {
  const content: any
  export default content
}

// vue-svg-loader
declare module '*.svg?inline' {
  const content: any
  export default content
}

// raw-loader
declare module '*.svg?raw' {
  const content: any
  export default content
}

// svg-sprite-loader
declare module '*.svg?sprite' {
  const content: any
  export default content
}
