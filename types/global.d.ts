import { providers } from 'ethers'
import { EthersFunctions } from './ethers'

declare module 'vue/types/vue' {
  interface Vue {
    $Web3: EthersFunctions
  }
}

declare global {
  interface Window {
    ethereum: providers.ExternalProvider
  }
}
