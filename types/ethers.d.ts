import { ethers, providers, Signer } from 'ethers'

export interface EthersjsPluginOptions {}

export interface EthersFunctions {
  ethers: typeof ethers
  getProvider: () => providers.Web3Provider
  setProvider: (provider: providers.ExternalProvider) => typeof ethers
  getSigner: () => Signer
  connect: () => Promise<string>
}
