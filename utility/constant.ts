enum StatusType {
  DEFAULT = 'default',
  ERROR = 'error',
  INFO = 'info',
  SUCCESS = 'success',
}

// eslint-disable-next-line import/prefer-default-export
export { StatusType }
